import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from './views/Home/Home';
import Work from './views/Work/Work';
import Refs from './views/Refs/Refs';
import Location from './views/Location/Location';
import Contact from './views/Contact/Contact';
import Header from './components/Header/Header';

class App extends React.Component {

  render() {
    return (
      <Router>
        <Header/>

        <Switch>
          <Route exact="exact" path={"/"} component={Home}/>
          <Route path={"/work"} component={Work}/>
          <Route path={"/refs"} component={Refs}/>
          <Route path={"/location"} component={Location}/>
          <Route path={"/contact"} component={Contact}/>
        </Switch>
      </Router>
  );
  };
}

export default App;
