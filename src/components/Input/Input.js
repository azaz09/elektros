import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
    tag: Tag,
    name,
    label,
    maxLength
  }) => ( < >

      <Tag
      type = {
        name === "submit" ? "submit" : Tag === "textarea" ? "" : "text"
      }
      name = {
        name
      }
      id = {
        name
      }
      placeholder = " "
      maxLength = {
        maxLength
      }
      />
      {label && < label htmlFor = {
        name
      } > {
        label
      } < /label>}
       </>
    );

    Input.propTypes = {
      tag: PropTypes.string,
      name: PropTypes.string.isRequired,
      label: PropTypes.string,
      maxLength: PropTypes.number,
    }

    Input.defaultProps = {
      tag: 'input',
      maxLength: 200,
    }

    export default Input;
