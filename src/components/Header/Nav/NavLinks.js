import React from 'react';
import Brand from '../../Brand/Brand';
import {NavLink} from 'react-router-dom';
import './NavLinks.css';
const NavLinks = ({brand, site}) => {
  return (<div className={"menu"}>
    {
      brand && <div>
          <Brand link={"/"}>{brand}</Brand>
        </div>
    }
    <ul>
      {
        site.map(prop => (<li key={prop.name}>
          <NavLink to={prop.link} className={'link'}>{prop.name}</NavLink>
        </li>))
      }

    </ul>
  </div>);
};

export default NavLinks;
