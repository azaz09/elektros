import React from 'react';
import NavLinks from './NavLinks';
import './Nav.css';
const Nav = () => {
  var links = [
    {
      'name': 'Działalność',
      'link': '/work'
    }, {
      'name': 'Referencje',
      'link': '/refs'
    }, {
      'name': 'Lokalizacja',
      'link': '/location'
    }, {
      'name': 'Kontakt',
      'link': '/contact'
    }
  ];
  return (<nav>
    <NavLinks brand={"ELEKTROS"} site={links}></NavLinks>
  </nav>);
}
export default Nav;
