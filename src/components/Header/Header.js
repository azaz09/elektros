import React from 'react';
import Nav from './Nav/Nav';
import Aside from '../Aside/Aside';
import './Header.css';

const Header = () => (<> < Nav /> <Aside/> < />);

export default Header;
