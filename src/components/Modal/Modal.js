import React from 'react';
import './Modal.css';
import {ModalContext} from '../../APIContext/APIContext.js';
const Modal = ({
  ...props
}) => (<ModalContext.Consumer>
  {(state) =>
    <div className={"modalBody"}>
    <div className={"close"} {...props}>^</div>
    <div className={"modalContent"}>
      <img src={state.refsContent[state.clickedRef].modalImg} alt="refs"/>
    </div>
  </div>
  }
</ModalContext.Consumer>


);

Modal.propTypes = {};

export default Modal;
