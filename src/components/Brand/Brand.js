import React from 'react';
import {NavLink} from 'react-router-dom';
import './Brand.css';
const Brand = ({
  children,
  link,
  ...props
}) => (<h1>
  <NavLink exact="exact" to={link} className={'neon'}>{children}</NavLink>
</h1>);

export default Brand;
