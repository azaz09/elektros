import React from 'react';
import PropTypes from 'prop-types';
import Brand from '../Brand/Brand';
import './Aside.css';
const Aside = () => (
  <aside className={"hero-container"}>
    <div className={"hero-overlay"}>
      <div className={"hero-content"}>
        <div>
          <p>
            <h1>TWORZENIE INSTALACJI ELEKTRYCZNYCH</h1>
          </p>
          </div>
        <div>
          <p>od projektu do implementacji</p>
        </div>
      </div>
    </div>
  </aside>
);

Aside.propTypes = {

};

export default Aside;
