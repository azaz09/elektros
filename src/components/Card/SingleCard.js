import React from 'react';
import './SingleCard.css';
import {ModalContext} from '../../APIContext/APIContext';
const SingleCard = ({
  children,
  adress,
  caption,
  tValue,
  color,
  ...props
}) => {
  const imgStyle = {
    background: color,
    background: 'url(' + adress + ') center no-repeat',
    backgroundSize: '100% 100%',
  }
  return (<ModalContext.Consumer>
    {
      (state) => <div className="card">
          <div className={"card-img"} style={imgStyle} onClick={state.modalOpen} data-tag={tValue}></div>
          <div className={"card-content"}>
            {children}
            <div className={"card-caption"}>
              <p>
                {caption}
              </p>
            </div>
          </div>
        </div>
    }
  </ModalContext.Consumer>);
}

export default SingleCard;
