import React from 'react';
import './Card.css';
import SingleCard from './SingleCard';
import {ModalContext} from '../../APIContext/APIContext';
const Cards = ({
  fnHandleClick,
  children,
  ...props
}) => {
  var i = 0;
return(<ModalContext.Consumer>
{(state)=>

    <div className="cards-container">
  {state.refsContent.map(ref=>(
    <SingleCard tValue={i++} caption={ref.caption} color={ref.color} adress={ref.imgAdress}>
      <p>{ref.content}</p>
      <p>{ref.repute}</p>
    </SingleCard>
  ), )}
  </div>
}
</ModalContext.Consumer>)
};

export default Cards;
