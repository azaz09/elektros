import React from 'react';
import PropTypes from 'prop-types';
import './Form.css';
import Input from '../Input/Input';
const Form = ({...props}) => (
<form method="POST" className={"contactForm"}>
<div className={"form-Control"}>
<Input name={"sub"} maxLength={255} label={"Temat"}/>
</div>
<div className={"form-Control"}>
<Input name="from" maxLength={255} label={"Od Kogo"}/>
</div>
<div><Input tag={"textarea"} name="text" maxLength={2500}/></div>
<Input name={"submit"} submit/>
</form>
);

Form.propTypes = {

};

export default Form;
