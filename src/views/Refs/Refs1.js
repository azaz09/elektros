import React, { Component } from 'react';

import Cards from '../../components/Card/Cards';
import Modal from '../../components/Modal/Modal';
import {ModalContext} from '../../APIContext/APIContext';
import PropTypes from 'prop-types';
class Refs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      modalOpen: this.handleModalOpen,
      refsContent: [
        {
          content: "Okresowa kontrola i pomiar instalacji elektrycznych przeprowadzony na dużą skalę.",
          repute: "Wykonane terminowo z należytą starannością, rzetelnie oraz z dużym zaangażowaniem.",
          caption: "~ Żagańskie Towarzystwo Budownictwa Społecznego Sp. z o.o.",
          color: "#228DFF",
          imgAdress: require("../../img/architecture.png"),
          modalImg: require("../../img/ref1.jpg")
        },
        {
          content: "Wymiana pionów zasilających mieszkalnych administrowanych przez Spółdzielnie mieszkaniową w Żarach.",
          repute: "Solidna praca, zrealizowana w terminie z zaangażowaniem i starannością.",
          caption: "~ Spółdzielnia mieszkaniowa - Żary.",
          color: "#228DFF",
          imgAdress: require("../../img/deal.png"),
          modalImg: require("../../img/ref2.jpg")
        },
        {
          content: "Pomiary instalacji elektrycznych i okresowa kontrola w budynkach mieszkalnych i lokalach użytkowych obejmująca 2699 lokali.",
          repute: "Stwierdzamy że firma ELEKTROS jest wiarygodnym i solidnym partnerem w wykonawstwie robót elektrycznych.",
          caption: "~ Spółdzielnia mieszkaniowa w Żaganiu.",
          color: "#228DFF",
          imgAdress: require("../../img/solid.png"),
          modalImg: require("../../img/ref3.jpg")
        },
        {
          content: "Stworzenie instalacji oświetlenia piwnic, dokonanie okresowej kontroli i pomiarów instalacji elektrycznych i odgromowych oraz ich odtworzenie w 656 lokalach mieszkalnych.",
          repute: "Powierzone zadania zostały wykonane w wymaganym czasie. Pomiary i instalacje sporządzono solidnie i zgodnie z założeniami.",
          caption: "",
          color: "",
          imgAdress: require("../../img/architecture.png"),
          modalImg: require("../../img/ref4.jpg")
        },
      ]
    }
  }


  handleModalOpen = () => {
    this.setState((prevState, props) => ({
      isModalOpen: !prevState.isModalOpen
    }));
    console.log(this.state.isModalOpen)
  }

  static propTypes = {

  }

  render() {
    return (
      <ModalContext.Provider value={this.state}>
      {this.state.isModalOpen && <Modal onClick={()=>this.handleModalOpen()} />}
      <Cards/>
      </ModalContext.Provider>
    );
  }
}

export default Refs;
